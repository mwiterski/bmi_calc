package com.hfad.bmi_calc

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.ekn.gruzer.gaugelibrary.ArcGauge
import com.ekn.gruzer.gaugelibrary.Range
import org.w3c.dom.Text
import kotlin.math.pow
import kotlin.math.roundToInt



class CalculatorFragment : Fragment(), View.OnClickListener {

    lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_calculator, container, false)

        var button: Button = rootView.findViewById(R.id.countBTN)

        button.setOnClickListener(this)
        return rootView;
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //TODO oprogramowac zapamietywanie stanu ui (tam gdzie potrzeba)
    }

    private fun initGauge(view: View, arcGauge: ArcGauge) {
        val range = Range()
        range.color = Color.parseColor("#00b20b")
        range.from = 0.0
        range.to = 25.0

        val range2 = Range()
        range2.color = Color.parseColor("#E3E500")
        range2.from = 25.0
        range2.to = 30.0

        val range3 = Range()
        range3.color = Color.parseColor("#ce0000")
        range3.from = 30.0
        range3.to = 50.0

        arcGauge.addRange(range)
        arcGauge.addRange(range2)
        arcGauge.addRange(range3)

        //set min max and current value
        arcGauge.minValue = 0.0
        arcGauge.maxValue = 50.0
    }

    fun count(view: View) {
        if (rootView != null) {
            var arcGauge: ArcGauge = rootView.findViewById(R.id.arcGauge)
            var heightET: EditText = rootView.findViewById(R.id.heightET)
            if (heightET.text.isBlank()) {
                heightET.error = getString(R.string.height_is_empty)
            }
            var mass: Int = 0
            var massET: EditText = rootView.findViewById(R.id.massET)
            var bmiValue: Double = massET.text.toString().toDouble() /
                    (heightET.text.toString().toDouble() / 100).pow(2)

            initGauge(rootView, arcGauge)

            arcGauge.value = (bmiValue * 100).roundToInt() / 100.toDouble()
        }
    }

    override fun onClick(v: View) {
        when (v?.id) {
            R.id.countBTN -> {
                count(rootView)
            }

            else -> {
            }
        }
    }
}
